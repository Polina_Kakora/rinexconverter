﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;

namespace Test
{
    class Program
    {
        enum Month : byte
        { JAN = 1, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC }

        static void Main(string[] args)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder tr = new StringBuilder();
            // Отобразит строковые представления чисел для культуры en-us:
            CultureInfo ci = new CultureInfo("en-us");

            //string fmt = "0.#E+##";
            string formatString = " {0,19:" + "D20" + "}";


            //double floating = 10761.937554456456456;
            double floating = 58;
            double floating1 = 295200;
            //Console.WriteLine("C: {0}",
            //        floating.ToString("C", ci));           // Отобразит "C: $10,761.94"
            Console.WriteLine("E: {0}",
                    floating.ToString("E03", ci));         // Отобразит "E: 1.076E+004"
            Console.WriteLine("F: {0}",
                    floating.ToString("F04", ci));         // Отобразит "F: 10761.9376"
            Console.WriteLine("G: {0}",
                    floating.ToString("G4", ci));           // Отобразит "G: 10761.937554"
            Console.WriteLine("N: {0}",
                    floating.ToString("N03", ci));         // Отобразит "N: 10,761.938"
            //Console.WriteLine("P: {0}",
            //        (floating / 10000).ToString("P02", ci)); // Отобразит "P: 107.62 %"
            Console.WriteLine("R: {0}",
                    floating.ToString("R", ci));           // Отобразит "R: 10761.937554"


            ////eph_rinex[i].IODE = double.Parse(str[0], System.Globalization.NumberStyles.AllowExponent | System.Globalization.NumberStyles.Number, ci);
            //sb.Append(eph_rinex2file[i].IODE.ToString("FAF.000000000000E+00", ci));//Custom Numeric Format Strings


            Console.WriteLine("{0,12:#.00}", floating);
            
            sb.Append(floating.ToString("FAF.000000000000E+00"+ "\n", ci));
            sb.Append(floating1.ToString("FAF.000000000000E+00", ci));
            
            sb.Replace("E+", "D+");
            sb.Replace("E-", "D-");
            sb.Replace("FAF.", "0.");
            Console.WriteLine(sb);
            Console.WriteLine("{0,4}", floating);
            Console.WriteLine(String.Format("{0,4}", floating));
            float flo = 5;
            float flod = 0.0007863185f;
            tr.Append(flo.ToString("0.000000000000E+00"+" ", ci));
            tr.Append(flo.ToString("0.000000000000E+00", ci));
            tr.Append(flo.ToString("0.000000000000E+00" + " ", ci));
            tr.Append(String.Format("{0,19:0.000000000000E+00}", flo));
            tr.Append(String.Format("{0,19:0.000000000000E+00}", flod));
            Console.WriteLine(tr);
            tr.Clear();
            tr.Append(flo.ToString("00"));
            tr.Append(String.Format("{0,2}", flo));
            Console.WriteLine(tr);
            Console.ReadLine();
    }
    }
}
