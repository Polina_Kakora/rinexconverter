﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToRinexConverter
{
    internal static class Rinex3_03
    {
        /// <summary>
        /// Формирование заголовка 
        /// </summary>
        internal static StringBuilder WriteHeaderGLONASS(DateTime creationTime)
        {
            StringBuilder fullEph = new StringBuilder();
            fullEph.AppendLine(String.Format(Constants.ci, "{0,9}", 3.02f).PadRight(20) + "N: GNSS NAV DATA".PadRight(20) + "R: GLONASS".PadRight(20) + "RINEX VERSION / TYPE");
            fullEph.AppendLine(Constants.nameOfProgram3_03.PadRight(20) + Constants.nameOfAgency3_03.PadRight(20) + creationTime.ToString("yyyyMMdd HHmmss").PadRight(16) + "GMT".PadRight(4) + "PGM / RUN BY / DATE ");
            fullEph.AppendLine(Constants.comment3_03.PadRight(60) + "COMMENT ");
            fullEph.AppendLine(Constants.comment3_03a.PadRight(60) + "COMMENT ");

            //fullEph.AppendLine("GPSA" + A0.ToString(".0000E+00" + "  ", ci).PadLeft(15) + A1.ToString(".0000E+00" + "  ", ci).PadLeft(12) + A2.ToString(".0000E+00" + "  ", ci).PadLeft(12) + A3.ToString(".0000E+00", ci).PadLeft(10) + "IONOSPHERIC CORR".PadLeft(23));
            //fullEph.AppendLine("GPSB" + B0.ToString(".0000E+00" + "  ", ci).PadLeft(15) + B1.ToString(".0000E+00" + "  ", ci).PadLeft(12) + B2.ToString(".0000E+00" + "  ", ci).PadLeft(12) + B3.ToString(".0000E+00", ci).PadLeft(10) + "IONOSPHERIC CORR".PadLeft(23));
            //fullEph.AppendLine("GPUT" + A_0.ToString(".0000000000E+00", ci).PadLeft(18) + A_1.ToString(".000000000E+00", ci).PadLeft(16) + timeUTC.ToString().PadLeft(7) + weeknum.ToString().PadLeft(5) + "TIME SYSTEM CORR".PadLeft(26));
            fullEph.AppendLine(Constants.leapSeconds.ToString().PadLeft(6) + "LEAP SECONDS        ".PadLeft(74));
            fullEph.AppendLine("END OF HEADER    ".PadLeft(77));

            return fullEph;
        }


        /// <summary>
        /// Формирование поля данных
        /// </summary>
        internal static StringBuilder WriteDataGLONASS(List<EphemerisGlonass> records)
        {
            StringBuilder fullEph = new StringBuilder();

            for (int i = 0; i < records.Count; i++)
            {
                fullEph.Append("R" + records[i].PRN.ToString("00", Constants.ci) + " " + (records[i].Year).ToString("0000") + " " + records[i].Month.ToString("00") + " ");
                fullEph.Append(records[i].Day.ToString("00") + " " + records[i].Hour.ToString("00") + " " + records[i].Minute.ToString("00") + " " + records[i].Second.ToString("00"));
                fullEph.AppendLine(records[i].SVClockBias.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].SVFreqBias.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].MessFrameTime.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));

                // ORBIT 1
                fullEph.Append(records[i].PosX.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].VelocityXDot.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].AccelerationX.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].Health.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 2
                fullEph.Append(records[i].PosY.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].VelocityYDot.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].AccelerationY.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].FreqNum.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 3
                fullEph.Append(records[i].PosZ.ToString("0.000000000000E+00", Constants.ci).PadLeft(23) + records[i].VelocityZDot.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].AccelerationZ.ToString("0.000000000000E+00", Constants.ci).PadLeft(19) + records[i].AgeInfo.ToString("0.000000000000E+00", Constants.ci).PadLeft(19));


                fullEph = fullEph.Replace("E-", "e-");
                fullEph = fullEph.Replace("E+", "e+");
            }
            return fullEph;
        }
    }
}
