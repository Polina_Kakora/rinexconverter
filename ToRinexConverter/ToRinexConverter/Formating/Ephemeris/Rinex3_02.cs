﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToRinexConverter
{
    internal static class Rinex3_02
    {
        #region Fake
        static float A0 = 0.00000001676f;
        static float A1 = -0.0000000149f;
        static float A2 = -0.0000000596f;
        static float A3 = 0.0000001788f;

        static float B0 = 129000;
        static float B1 = -180200;
        static float B2 = 65540;
        static float B3 = 131100;

        static double A_0 = -0.00000000465661287308d;
        static double A_1 = -0.000000000000008881784197d;
        static int timeUTC = 233472;
        static int weeknum = 1875;
        #endregion

        /// <summary>
        /// Формирование заголовока V3.02
        /// </summary>
        internal static string WriteHeaderGPS(DateTime timeFileCreation)
        {
            StringBuilder fullEph = new StringBuilder();
            fullEph.AppendLine(String.Format(Constants.ci, "{0,9}", 3.02f).PadRight(20) + "N: GNSS NAV DATA".PadRight(20) + "G: GPS".PadRight(20) + "RINEX VERSION / TYPE");
            fullEph.AppendLine(Constants.nameOfProgram3_02.PadRight(40) + timeFileCreation.ToString("yyyyMMdd HHmmss").PadRight(16) + "UTC".PadRight(4) + "PGM / RUN BY / DATE");
            fullEph.AppendLine("COMMENT".PadLeft(67));
            fullEph.AppendLine("COMMENT".PadLeft(67));
            fullEph.AppendLine("COMMENT".PadLeft(67));

            fullEph.AppendLine("GPSA" + A0.ToString(".0000E+00" + "  ", Constants.ci).PadLeft(15) + A1.ToString(".0000E+00" + "  ", Constants.ci).PadLeft(12) + A2.ToString(".0000E+00" + "  ", Constants.ci).PadLeft(12) + A3.ToString(".0000E+00", Constants.ci).PadLeft(10) + "IONOSPHERIC CORR".PadLeft(23));
            fullEph.AppendLine("GPSB" + B0.ToString(".0000E+00" + "  ", Constants.ci).PadLeft(15) + B1.ToString(".0000E+00" + "  ", Constants.ci).PadLeft(12) + B2.ToString(".0000E+00" + "  ", Constants.ci).PadLeft(12) + B3.ToString(".0000E+00", Constants.ci).PadLeft(10) + "IONOSPHERIC CORR".PadLeft(23));
            fullEph.AppendLine("GPUT" + A_0.ToString(".0000000000E+00", Constants.ci).PadLeft(18) + A_1.ToString(".000000000E+00", Constants.ci).PadLeft(16) + timeUTC.ToString().PadLeft(7) + weeknum.ToString().PadLeft(5) + "TIME SYSTEM CORR".PadLeft(26));
            fullEph.AppendLine(Constants.leapSeconds.ToString().PadLeft(6) + "LEAP SECONDS".PadLeft(66));
            fullEph.AppendLine("END OF HEADER".PadLeft(73));

            return fullEph.ToString();
        }


        /// <summary>
        /// Формирование поля данных
        /// </summary>
        internal static string WriteData(List<EphemerisGps> records)
        {
            StringBuilder fullEph = new StringBuilder();

            for (int i = 0; i < records.Count; i++)
            {
                fullEph.Append("G" + records[i].PRN.ToString("00", Constants.ci) + " " + (records[i].Year).ToString() + " " + records[i].Month.ToString("00") + " ");
                fullEph.Append(records[i].Day.ToString("00") + " " + records[i].Hour.ToString("00") + " " + records[i].Minute.ToString("00") + " " + records[i].Second.ToString("00"));
                fullEph.AppendLine(records[i].SVClockBias.ToString(".000000000000E+00", Constants.ci).PadLeft(19) + records[i].SVClockDrift.ToString(".000000000000E+00", Constants.ci).PadLeft(19) + records[i].SVClockDriftRate.ToString(".000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 1
                fullEph.Append(records[i].IODE.ToString(".000000000000E+00", Constants.ci).PadLeft(23) + records[i].C_rs.ToString(".000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].delta_n.ToString(".000000000000E+00", Constants.ci).PadLeft(19) + records[i].M_0.ToString(".000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 2
                fullEph.Append(records[i].C_uc.ToString(".000000000000E+00", Constants.ci).PadLeft(23) + records[i].e.ToString(".000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].C_us.ToString(".000000000000E+00", Constants.ci).PadLeft(19) + records[i].sqrt_A.ToString(".000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 3
                fullEph.Append(records[i].T_oe.ToString(".000000000000E+00", Constants.ci).PadLeft(23) + records[i].C_ic.ToString(".000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].OMEGA_0.ToString(".000000000000E+00", Constants.ci).PadLeft(19) + records[i].C_is.ToString(".000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 4
                fullEph.Append(records[i].i_0.ToString(".000000000000E+00", Constants.ci).PadLeft(23) + records[i].C_rc.ToString(".000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].omega.ToString(".000000000000E+00", Constants.ci).PadLeft(19) + records[i].OMEGADOT.ToString(".000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 5
                fullEph.Append(records[i].IDOT.ToString(".000000000000E+00", Constants.ci).PadLeft(23) + records[i].codeL2.ToString(".000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].weeknum.ToString(".000000000000E+00", Constants.ci).PadLeft(19) + records[i].L2Pdata.ToString(".000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 6
                fullEph.Append(records[i].SVacc.ToString(".000000000000E+00", Constants.ci).PadLeft(23) + records[i].SV_health.ToString(".000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(records[i].T_GD.ToString(".000000000000E+00", Constants.ci).PadLeft(19) + records[i].IODC.ToString(".000000000000E+00", Constants.ci).PadLeft(19));
                // ORBIT 7
                fullEph.Append(records[i].HOW.ToString(".000000000000E+00", Constants.ci).PadLeft(23) + records[i].fit_interval.ToString(".000000000000E+00", Constants.ci).PadLeft(19));
                fullEph.AppendLine(0.ToString(".000000000000E+00", Constants.ci).PadLeft(19) + 0.ToString(".000000000000E+00", Constants.ci).PadLeft(19));
            }
            return fullEph.ToString();
        }
    }
}
