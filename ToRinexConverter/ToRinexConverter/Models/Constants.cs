﻿
using System.Globalization;

namespace ToRinexConverter
{
    internal static class Constants
    {
        internal static readonly CultureInfo ci = new CultureInfo("en-us");

        internal static string logGps = "\\Log_Gps";
        internal static string logGlonass = "\\Log_Glonass";

        internal static string formatAlmanacGps = ".AL3";
        internal static string formatAlmanacGlonass = ".Agl";
        internal static string formatEphemerisGps = ".15n";
        internal static string formatEphemerisGlonass = ".rnx";

        internal static string constNameAlmanac = "almanac";
        internal static string constNameEphemeris = "ephemeris";

        internal static int leapSeconds = 18; //надо как-то отслеживать и корректировать значение

        #region Gps
        internal static string almanacInside = "CURRENT.ALM";
        #endregion

        #region Version 2
        //[StringLength(20)]
        public static string nameOfProgram2 = "TRIMBLE SUP V.1";
        //[StringLength(20)]
        public static string nameOfAgency2 = "SECRET";
        //[StringLength(60)]
        public static string comment2 = "IGS BROADCAST EPHEMERIS FILE";
        //DateTime timeFileCreation;

        public static string nameOfProgram2Glo = "ASRINEXG V1.1.0 VM";
        //[StringLength(20)]
        public static string nameOfAgency2Glo = "AIUB";
        //[StringLength(60)]
        public static string comment2Glo = "STATION ZIMMERWALD";
        #endregion

        #region Version 3.02
        //float rinexVersion3 = 3.02f;
        //[StringLength(20)]
        public static string nameOfProgram3_02 = "TPP 4.1.3";
        ////[StringLength(20)]
        //public static string nameOfAgency3_02 = "SECRET";
        ////[StringLength(60)]
        //public static string comment3_02 = "IGS BROADCAST EPHEMERIS FILE";
        ////DateTime timeFileCreation;
        #endregion


        #region Version 3.03
        //[StringLength(20)]
        public static string nameOfProgram3_03 = "BCEmerge";
        //[StringLength(20)]
        public static string nameOfAgency3_03 = "congo";
        //[StringLength(60)]
        public static string comment3_03 = "based on CONGO and MGEX tracking data";
        //[StringLength(60)]
        public static string comment3_03a = "DLR/GSOC: O. Montenbruck; P. Steigenberger";
        //DateTime timeFileCreation;
        #endregion
    }
}
