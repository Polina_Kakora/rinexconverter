﻿using System;

namespace ToRinexConverter
{
    public class EphemerisGps : ICloneable
    {
        DateTime dtGPSEp = new DateTime(1980, 1, 6);
        public DateTime dtTmp; 

        public byte PRN;                    // 0    tsip
        public double Year;
        public double Month;
        public double Day;
        public double Hour;
        public double Minute;
        public double Second;
        public double SVClockBias;          // 12   tsip
        public double SVClockDrift;         // 11   tsip
        public double SVClockDriftRate;     // 10   tsip
        // ORBIT 1
        public double IODE;                 // 14   tsip
        public double C_rs;                 // 16   tsip
        public double delta_n;              // 17   tsip
        public double M_0;                  // 18   tsip
        // ORBIT 2
        public double C_uc;                 // 19   tsip
        public double e;                    // 20   tsip
        public double C_us;                 // 21   tsip
        public double sqrt_A;               // 22   tsip
        // ORBIT 3
        public double T_oe;                 // 9,23 tsip
        public double C_ic;                 // 24   tsip
        public double OMEGA_0;              // 25   tsip
        public double C_is;                 // 26   tsip
        // ORBIT 4
        public double i_0;                  // 27   tsip
        public double C_rc;                 // 28   tsip
        public double omega;                // 29   tsip
        public double OMEGADOT;             // 30   tsip
        // ORBIT 5
        public double IDOT;                 // 31   tsip
        public double codeL2;               // 3    tsip
        public double weeknum;              // 2    tsip  = tsip + 1024
        public double L2Pdata;              // 4    tsip
        // ORBIT 6
        public double SVacc;                // 13   tsip
        public double SV_health;            // 6    tsip
        public double T_GD;                 // 8    tsip
        public double IODC;                 // 7    tsip
        // ORBIT 7
        public double HOW;                  //      tsip -
        public double fit_interval;         // 15   tsip

        public EphemerisGps(TSIP.Ephemeris ephTSIP)
        {
            PRN = ephTSIP.sv_number;

            dtTmp = dtGPSEp.AddDays(7 * (ephTSIP.weeknum + 1024*2)).AddSeconds(ephTSIP.t_oe); //разобраться
            Year = dtTmp.Year;
            Month= dtTmp.Month;
            Day= dtTmp.Day;
            Hour= dtTmp.Hour;
            Minute= dtTmp.Minute;
            Second= dtTmp.Second;

            SVClockBias = ephTSIP.a_f0;   
            SVClockDrift = ephTSIP.a_f1;    
            SVClockDriftRate = ephTSIP.a_f2; 
                                             
            // ORBIT 1
            IODE = ephTSIP.IODE;              
            C_rs = ephTSIP.C_rs;             
            delta_n = ephTSIP.delta_n;  //radians
            M_0 = ephTSIP.M_0 ;    
            
            // ORBIT 2
            C_uc = ephTSIP.C_uc;            
            e = ephTSIP.e;                 
            C_us = ephTSIP.C_us;              
            sqrt_A = ephTSIP.sqrt_A;            

            // ORBIT 3
            T_oe = ephTSIP.t_oe;              
            C_ic = ephTSIP.C_ic;            
            OMEGA_0 = ephTSIP.OMEGA_0;
            C_is = ephTSIP.C_is;            
                                               
            // ORBIT 4
            i_0 = ephTSIP.i_o ;                
            C_rc = ephTSIP.C_rc;              
            omega = ephTSIP.omega ;
            OMEGADOT = ephTSIP.OMEGADOT;           
                                                
            // ORBIT 5
            IDOT = ephTSIP.IDOT;               
            codeL2 = ephTSIP.codeL2;             
            weeknum = ephTSIP.weeknum+1024*2;          //каждые 1024 недели идет сбрасывание (~19,6 лет)
            L2Pdata = ephTSIP.L2Pdata;            
                                               
            // ORBIT 6
            SVacc = ephTSIP.SVacc;               
            SV_health = ephTSIP.SV_health;          
            T_GD = ephTSIP.T_GD;               
            IODC = ephTSIP.IODC;              
                                                 
            // ORBIT 7
            HOW = ephTSIP.t_oe;                //хз как заполнять    Transmission time of message sec of GPS week, derived e.g. from Z-count in Hand Over Word (HOW)
            fit_interval = ephTSIP.fit_interval;
            
        }

        public EphemerisGps(ClientServerLib.Ephemeries ephTSIP)
        {
            PRN = ephTSIP.sv_number;

            dtTmp = dtGPSEp.AddDays(7 * (ephTSIP.weeknum + 1024 * 2)).AddSeconds(ephTSIP.t_oe); //разобраться
            Year = dtTmp.Year;
            Month = dtTmp.Month;
            Day = dtTmp.Day;
            Hour = dtTmp.Hour;
            Minute = dtTmp.Minute;
            Second = dtTmp.Second;

            SVClockBias = ephTSIP.a_f0;
            SVClockDrift = ephTSIP.a_f1;
            SVClockDriftRate = ephTSIP.a_f2;

            // ORBIT 1
            IODE = ephTSIP.IODE;
            C_rs = ephTSIP.C_rs;
            delta_n = ephTSIP.delta_n;  //radians
            M_0 = ephTSIP.M_0;

            // ORBIT 2
            C_uc = ephTSIP.C_uc;
            e = ephTSIP.e;
            C_us = ephTSIP.C_us;
            sqrt_A = ephTSIP.sqrt_A;

            // ORBIT 3
            T_oe = ephTSIP.t_oe;
            C_ic = ephTSIP.C_ic;
            OMEGA_0 = ephTSIP.OMEGA_0;
            C_is = ephTSIP.C_is;

            // ORBIT 4
            i_0 = ephTSIP.i_o;
            C_rc = ephTSIP.C_rc;
            omega = ephTSIP.omega;
            OMEGADOT = ephTSIP.OMEGADOT;

            // ORBIT 5
            IDOT = ephTSIP.IDOT;
            codeL2 = ephTSIP.codeL2;
            weeknum = ephTSIP.weeknum + 1024 * 2;          //каждые 1024 недели идет сбрасывание (~19,6 лет)
            L2Pdata = ephTSIP.L2Pdata;

            // ORBIT 6
            SVacc = ephTSIP.SVacc;
            SV_health = ephTSIP.SV_health;
            T_GD = ephTSIP.T_GD;
            IODC = ephTSIP.IODC;

            // ORBIT 7
            HOW = ephTSIP.t_oe;                //хз как заполнять    Transmission time of message sec of GPS week, derived e.g. from Z-count in Hand Over Word (HOW)
            fit_interval = ephTSIP.fit_interval;

        }


        public EphemerisGps(GeostarBinaryProtocol.EphemerisGps ephGeos)
        {
            PRN = ephGeos.sv_number;

            dtTmp = dtGPSEp.AddDays(7 * (ephGeos.weeknum + 1024 * 2)).AddSeconds(ephGeos.t_oe); //разобраться
            Year = dtTmp.Year;
            Month = dtTmp.Month;
            Day = dtTmp.Day;
            Hour = dtTmp.Hour;
            Minute = dtTmp.Minute;
            Second = dtTmp.Second;

            SVClockBias = ephGeos.a_f0;
            SVClockDrift = ephGeos.a_f1;
            SVClockDriftRate = ephGeos.a_f2;

            // ORBIT 1
            //IODE = (int)ephGeos.IODE;
            IODE = ephGeos.IODC;
            C_rs = ephGeos.C_rs;
            delta_n = ephGeos.delta_n * Math.PI;  //radians
            M_0 = ephGeos.M_0 * Math.PI;

            // ORBIT 2
            C_uc = ephGeos.C_uc;
            e = ephGeos.e;
            C_us = ephGeos.C_us;
            sqrt_A = ephGeos.sqrt_A;

            // ORBIT 3
            T_oe = ephGeos.t_oe;
            C_ic = ephGeos.C_ic;
            OMEGA_0 = ephGeos.OMEGA_0 * Math.PI;
            C_is = ephGeos.C_is;

            // ORBIT 4
            i_0 = ephGeos.i_o * Math.PI;
            C_rc = ephGeos.C_rc;
            omega = ephGeos.omega * Math.PI;
            OMEGADOT = ephGeos.OMEGADOT * Math.PI;

            // ORBIT 5
            IDOT = ephGeos.IDOT * Math.PI;
            codeL2 = 0; //хз
            weeknum = ephGeos.weeknum + 1024 * 2;          //каждые 1024 недели идет сбрасывание (~19,6 лет)
            L2Pdata = 0; //хз

            // ORBIT 6
            SVacc = (int)ephGeos.SVacc;
            SV_health = (int)ephGeos.SV_health;
            T_GD = ephGeos.T_GD;
            IODC = ephGeos.IODC;

            // ORBIT 7
            HOW = ephGeos.t_oe;                //хз как заполнять    Transmission time of message sec of GPS week, derived e.g. from Z-count in Hand Over Word (HOW)
            fit_interval = 0; //хз

        }

        public EphemerisGps()
        {
        }

        public interface ICloneable
        {
            object Clone();
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }


        public void CountDataTime()
        {
            dtTmp = dtGPSEp.AddDays(7 * (weeknum)).AddSeconds(T_oe);
            Year = dtTmp.Year;
            Month = dtTmp.Month;
            Day = dtTmp.Day;
            Hour = dtTmp.Hour;
            Minute = dtTmp.Minute;
            Second = dtTmp.Second;
        }
    }
}
